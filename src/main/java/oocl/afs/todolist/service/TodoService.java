package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoJPARepository;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;
import oocl.afs.todolist.exception.TodoNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    private final TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public List<TodoResponse> findAll() {
        List<Todo> todos = todoJPARepository.findAll();
        return todos.stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public TodoResponse getTodoById(Long id) {
        return TodoMapper.toResponse(todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }


    public TodoResponse updateTodo(Long id, TodoRequest todoRequest) {
        Todo toBeUpdatedTodo = todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (todoRequest.getName() != null) {
            toBeUpdatedTodo.setName(todoRequest.getName());
        }
        if (todoRequest.getDone() != null) {
            toBeUpdatedTodo.setDone(todoRequest.getDone());
        }
        return TodoMapper.toResponse(todoJPARepository.save(toBeUpdatedTodo));
    }

    public void deleteTodo(Long id) {
        todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        todoJPARepository.deleteById(id);
    }


    public TodoResponse addTodoItem(TodoRequest request) {
        return TodoMapper.toResponse(todoJPARepository.save(TodoMapper.toEntity(request)));
    }

}
