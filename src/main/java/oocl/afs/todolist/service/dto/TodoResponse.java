package oocl.afs.todolist.service.dto;

import lombok.Data;

@Data
public class TodoResponse {
    private Long id;

    private String name;

    private Boolean done;
}
