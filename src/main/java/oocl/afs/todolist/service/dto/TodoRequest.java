package oocl.afs.todolist.service.dto;

import lombok.Data;

@Data
public class TodoRequest {
    private String name;

    private Boolean done;
}
