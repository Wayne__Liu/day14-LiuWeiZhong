package oocl.afs.todolist.service.mapper;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {

    public static Todo toEntity(TodoRequest todoRequest){
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo){
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo,todoResponse);
        return todoResponse;
    }
}
