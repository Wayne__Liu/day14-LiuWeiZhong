package oocl.afs.todolist.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoJPARepository;
import oocl.afs.todolist.service.dto.TodoRequest;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    TodoController todoController;

    @Autowired
    TodoJPARepository todoJPARepository;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }

    @Test
    void should_return_all_todo_when_get_all_todo_given_a_todo_save() throws Exception {
        Todo todo = TodoMapper.toEntity(getTodoRequest());
        Todo saveTodo = todoJPARepository.save(todo);
        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(saveTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(saveTodo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").isBoolean());
    }

    @Test
    void should_return_todo_when_getTodoById_given_id() throws Exception{
        Todo todoItem = getTodo();
        Todo saveTodoItem = todoJPARepository.save(todoItem);

        mockMvc.perform(get("/todo/{id}", saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(saveTodoItem.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(saveTodoItem.getDone()));
    }

    @Test
    void should_return_updated_todo_when_updateTodo_given_id_and_request_todo() throws Exception {
        Todo todoItem = getTodo();
        Todo savedTodoItem = todoJPARepository.save(todoItem);
        TodoRequest todoRequestItem = getTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoRequestItem);
        mockMvc.perform(put("/todo/{id}", savedTodoItem.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(200));
        Optional<Todo> optionalEmployee = todoJPARepository.findById(savedTodoItem.getId());
        assertTrue(optionalEmployee.isPresent());
        Todo updatedTodo = optionalEmployee.get();
        Assertions.assertEquals(todoRequestItem.getName(), updatedTodo.getName());
        Assertions.assertEquals(todoRequestItem.getDone(), updatedTodo.getDone());
        Assertions.assertEquals(savedTodoItem.getId(), updatedTodo.getId());
    }

    @Test
    void should_return_nothing_when_deleteTodo_given_Todo_id() throws Exception {
        Todo todoItem = getTodo();
        Todo saveTodoItem = todoJPARepository.save(todoItem);

        mockMvc.perform(delete("/todo/{id}", saveTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Assertions.assertTrue(todoJPARepository.findById(saveTodoItem.getId()).isEmpty());
    }
    @Test
    void should_return_todo_when_create_todo_item_given_request_todo() throws Exception {
        TodoRequest todoItem = getTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todoItem);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoItem.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.getDone()));
    }


    private static TodoRequest getTodoRequest() {
        TodoRequest newTodo = new TodoRequest();
        newTodo.setName("finish homework");
        newTodo.setDone(true);
        return newTodo;
    }

    private static Todo getTodo() {
        Todo newTodo = new Todo();
        newTodo.setName("finish homework withRequest");
        newTodo.setDone(true);
        return newTodo;
    }
}
