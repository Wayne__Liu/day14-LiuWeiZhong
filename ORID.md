## **O**：

-  In Code Review, I focused on responsive layout, but the results were not satisfactory. The interface I created looks like a responsive layout, but there were issues in actual operation. This is because I have not written any relevant code before, resulting in an abnormal responsive layout.
-  In the front-end and back-end integration, we have written a complete back-end and completed the calls between the front-end and back-end using real API. This is not difficult for me, it's equivalent to reviewing last week's content and deepening my impression of the entire process.
-  I also learned methods to solve cross domain problems, allowing cross domain configuration through Java Cors, and learned to write some detailed configurations.
-  In addition, I also have a deeper understanding of concepts related to agile development, such as User story, user journeys, elevator speeches, MVP products, etc.
-  Overall, in today's learning, I have accumulated skills in front-end development and back-end collaboration, and experienced the concepts related to agile development and their impact on development.



## **R：**

- bumper harvest


## **I：**

- I found myself not familiar with responsive layout enough. I spent some time trying it out last night, but still made many mistakes.


## **D：**

- In the future, I will continue to strengthen my learning of responsive layout and styles, and strive to improve my development skills.